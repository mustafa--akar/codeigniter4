<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateSettings extends Migration
{

	public function up()
	{
			$this->forge->addField([
					'id'          => [
							'type'           => 'INT',
							'constraint'     => 11,
							'unsigned'       => true,
							'auto_increment' => true,
					],
					'company_name'       => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
					],
					'phone' => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
							'null'           => true,
					],
					'fax' => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
							'null'           => true,
					],
					'address' => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
							'null'           => true,
					],
					'about_us' => [
							'type'           => 'TEXT',
							'null'           => true,
					],
					'mission' => [
							'type'           => 'TEXT',
							'null'           => true,
					],
					'vision' => [
							'type'           => 'TEXT',
							'null'           => true,
					],
					'motto' => [
							'type'           => 'TEXT',
							'null'           => true,
					],
					'email' => [
							'type'           => 'VARCHAR',
							'constraint'     => '255',
							'null'           => true,
					],					
					'facebook' => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
						'null'           => true,
					],					
					'twitter' => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
						'null'           => true,
					],					
					'instagram' => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
						'null'           => true,
					],					
					'linkedin' => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
						'null'           => true,
					],					
					'logo' => [
						'type'           => 'VARCHAR',
						'constraint'     => '255',
						'null'           => true,
					],					
					'created_at' => [
						'type'           => 'DATETIME',						
						'null'           => true,
					],					
					'updated_at' => [
						'type'           => 'DATETIME',						
						'null'           => true,
					],
				
			]);
			$this->forge->addKey('id', true);
			$this->forge->createTable('settings');
	}

	public function down()
	{
			$this->forge->dropTable('settings');
	}

}
