<?php

function selftLink($str)
{
	// Tr karakterler ile birlikte küçültme işlemi

	$str = mb_strtolower($str, 'UTF-8');
	$str = str_replace(
		['ı', 'ğ', 'ç', 'ö', 'ş'],
		['i', 'g', 'c', 'o', 's'],
		$str
	);

	// normal karakterler ve sayılar hariç herşeyi - işaretine çevir
	$str = preg_replace('/[^a-z0-9]/', '-', $str);
	

	//birden fazla - işaretlerini tek bir - ye çevir
	$str = preg_replace('/-+/', '-', $str);


	// en sonda ve en baştaki - işaretlerinde kurtul
	return trim($str, '-');



}


