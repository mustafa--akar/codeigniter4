<?php namespace App\Controllers;

class Dashboard extends BaseController
{


	public function index()
	{		

		$data = ['uri' => service('uri')];
        
		return view('panel/dashboard', $data);
	
	}

	//--------------------------------------------------------------------

}
